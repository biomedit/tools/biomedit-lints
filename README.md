# biomedit-lints

A collection of pylint checks used in the biomedit projects.
Currently.

## Checks for the typing module

These checks are a step towards [PEP 585 -- Type Hinting Generics In
Standard
Collections](https://www.python.org/dev/peps/pep-0585/#backwards-compatibility).

Usage: `pylint --load-plugins=biomedit_lints.pep585_pre ...`.

* `typing-prefix`: Assert that `import typing` / `typing.Dict`, ... is
  not used.
  Instead import all needed types with `from typing import ...`.

* `generic-typing-type`: Assert that types from the typing module are not used without specializing
  (e.g. `Dict` in contrast to `Dict[int, int]`). When no specialization is used,
  prefer the builtin types. E.g. `dict`.

## PEP 585 checker

A deprecation checker for [PEP 585 -- Type Hinting Generics In
Standard
Collections](https://www.python.org/dev/peps/pep-0585/#backwards-compatibility).

Usage: `pylint --load-plugins=biomedit_lints.pep585 ...`.
