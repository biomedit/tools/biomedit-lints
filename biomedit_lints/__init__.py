"""Custom pylint checks used in the biomedit projects"""

from .pep585_pre import register

__all__ = ["register"]
