"""pylint check to enforce usage of PEP 585"""

from pylint.interfaces import IAstroidChecker
from pylint.checkers import BaseChecker

TYPE_MAPPING = {
    "List": "list",
    "Tuple": "tuple",
    "Set": "set",
    "Dict": "dict",
    "FrozenSet": "frozenset",
    "Type": "type",
    "Iterable": "collections.abc.Iterable",
    "Iterator": "collections.abc.Iterator",
    "Generator": "collections.abc.Generator",
    "Container": "collections.abc.Container",
    "Collection": "collections.abc.Collection",
    "Callable": "collections.abc.Callable",
    "Sequence": "collections.abc.Sequence",
    "ByteString": "collections.abc.ByteString",
    "ItemsView": "collections.abc.ItemsView",
    "KeysView": "collections.abc.KeysView",
    "Mapping": "collections.abc.Mapping",
    "MappingView": "collections.abc.MappingView",
    "MutableMapping": "collections.abc.MutableMapping",
    "MutableSequence": "collections.abc.MutableSequence",
    "MutableSet": "collections.abc.MutableSet",
    "ValuesView": "collections.abc.ValuesView",
    "Reversible": "collections.abc.Reversible",
    "Coroutine": "collections.abc.Coroutine",
    "AsyncGenerator": "collections.abc.AsyncGenerator",
    "AsyncIterable": "collections.abc.AsyncIterable",
    "AsyncIterator": "collections.abc.AsyncIterator",
    "Awaitable": "collections.abc.Awaitable",
    "DefaultDict": "collections.defaultdict",
    "OrderedDict": "collections.OrderedDict",
    "ChainMap": "collections.ChainMap",
    "Counter": "collections.Counter",
    "Deque": "collections.deque",
    # "typing.io.IO": "typing.IO",
    # "typing.io.TextIO": "typing.TextIO",
    # "typing.io.BinaryIO": "typing.BinaryIO",
    "Pattern": "re.Pattern",
    "Match": "re.Match",
    "AbstractSet": "collections.abc.Set",
    "ContextManager": "contextlib.AbstractContextManager",
    "AsyncContextManager": "contextlib.AbstractAsyncContextManager",
}


class Pep585Check(BaseChecker):
    """Combines several rules into one pylint check"""

    __implements__ = IAstroidChecker

    name = "pep-585-checker"
    priority = -1

    DEPRECIATED_TYPE_HINT = "depreciated-type-hint"

    msgs = {
        "C9003": (
            "Using deprecated type hint typing.%s. Use %s instead.",
            DEPRECIATED_TYPE_HINT,
            "",
        ),
    }
    options = ()

    def visit_annassign(self, node):
        """Assert that a PEP 585 type hint is used"""
        type_hint = node.annotation.as_string().split("[")[0]
        if type_hint.startswith("typing."):
            type_hint = type_hint[len("typing.") :]
        replacement = TYPE_MAPPING.get(type_hint)
        if replacement is not None:
            self.add_message(
                self.DEPRECIATED_TYPE_HINT, node=node, args=(type_hint, replacement)
            )


def register(linter):
    """required method to auto register this checker"""
    linter.register_checker(Pep585Check(linter))
