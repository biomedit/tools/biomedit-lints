"""pylint check to assure some conventions about the typing module"""

from pylint.interfaces import IAstroidChecker
from pylint.checkers import BaseChecker


class Pep585PreCheck(BaseChecker):
    """Combines several rules into one pylint check"""

    __implements__ = IAstroidChecker

    name = "pep585-pre-checker"
    priority = -1

    TYPING_PREFIX = "typing-prefix"
    GENERIC_TYPING_TYPE = "generic-typing-type"

    msgs = {
        "C9001": (
            "Use from typing import ... instead",
            TYPING_PREFIX,
            "",
        ),
        "C9002": (
            "Use a builtin python type instead",
            GENERIC_TYPING_TYPE,
            "",
        ),
    }
    options = ()

    def visit_import(self, node):
        """Assert that `import typing` / `typing.Dict`, ... is not used."""
        if "typing" in {name for name, _ in node.names}:
            self.add_message(self.TYPING_PREFIX, node=node)

    def visit_annassign(self, node):
        """Assert that types from the typing module are not used without specializing
        (e.g. `Dict` in contrast to `Dict[int, int]`). When no specialization is used,
        prefer the builtin types. E.g. `dict`"""
        type_hint = node.annotation.as_string()
        if "[" not in type_hint and type_hint in (
            "Dict",
            "Tuple",
            "Sequence",
            "List",
            "Type",
        ):
            self.add_message(self.GENERIC_TYPING_TYPE, node=node)


def register(linter):
    """required method to auto register this checker"""
    linter.register_checker(Pep585PreCheck(linter))
